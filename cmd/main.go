package main

import (
	"context"
	"fmt"
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
	"os"
	"os/signal"
)

func main()  {

	quitSignal := make(chan os.Signal, 1)
	signal.Notify(quitSignal, os.Interrupt)

  srv := newServer(":8487")

  defer last(srv)
	ctx, cancel := context.WithCancel(context.Background())
	go func() {
		OSCall := <-quitSignal
		log.Println(fmt.Sprintf("System Call: %+v", OSCall))
		cancel()
	}()
	go func() {
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			panic(err)
		}
	}()

	<-ctx.Done()
}

func last(rest *http.Server)  {
	log.Println("HTTP is shutting down")
	ctxShutDown, cancel := context.WithTimeout(context.Background(), 10)
	defer cancel()
	if err := rest.Shutdown(ctxShutDown); err != nil {
		log.Fatal(fmt.Sprintf("server Shutdown Failed:%s", err))
		if err == http.ErrServerClosed {
			err = nil
		}
		return
	}
	log.Println("HTTP is shut down")
}

type Server struct {
	router *gin.Engine
}

func newServer(port string)  *http.Server {
		r := gin.New()
		srv := &Server{

		router: r,
	}
		srv.endpoints()
		httpServer := &http.Server{
		Addr:    port,
		Handler: srv,
	}
		log.Println(fmt.Sprintf("HTTP server is initialized on port: %v", port))
		return httpServer
}

func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	s.router.ServeHTTP(w, r)
}

func (s *Server) endpoints()  {
  s.router.GET("/ping1",s.ping1())
	s.router.GET("/ping2",s.ping2())
	s.router.GET("/ping3",s.ping3())
}

func (s *Server) ping1() gin.HandlerFunc {
	return func(c *gin.Context) {
		test1()
	}
}

func (s *Server) ping2() gin.HandlerFunc {
	return func(c *gin.Context) {
		test2()
	}
}

func (s *Server) ping3() gin.HandlerFunc {
	return func(c *gin.Context) {
		test3()
	}
}