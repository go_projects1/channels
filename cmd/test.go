package main

import (
	"fmt"
	"time"
)

var chn = make(chan string)
var chn2 = make(chan string)
func test1()  {
	var res2 string
	fmt.Println("Starting...")
	for {
		select {
		 case  res := <- chn:
			 fmt.Println("received:"+res)
		case res2 = <- chn2:
			fmt.Println("received2:"+res2)
			break
		default:
			fmt.Println("no received")
		}
		time.Sleep(5*time.Second)
		fmt.Println(time.Now().String())
		if res2 != ""{
			fmt.Println("prepare to exit")
			break
		}
	}

	fmt.Println("exited")
}

func test2()  {
	fmt.Println("processing")
	chn <- "process"
}

func test3()  {
	fmt.Println("stopping")
	chn2 <- "stop"
}